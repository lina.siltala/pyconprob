
# The functions in this file are the one which need to call some function from pyagrum python package.

import numpy as np
import pandas as pd

import pyAgrum as gum
import pyAgrum.lib.notebook as gnb

#helper function to add  node
# more functions about influence diagram in the link:
# https://pyagrum.readthedocs.io/en/stable/influenceDiagram.html?highlight=influence
def add_node(model,data,col,node_type = 'chance_node'):
    #TODO:
    #1, data can be csv, or df.
    #2, handle if col is val not cat, 
    #3, handle if there is weird values in the col, e.g. nan, comorbidity data is a good example
    #4. chagne it to add_nodes so it is used for decision node and utility node.
    #5. fix return, it should be model not IDmodel
    #6. handle when node already exist.
    
    """create chance nodes from the data. 
    Inputs:
        model:     influence diagram or decision network which is created with pyargum.
        data:      data source which is used to create chance node.
        col:       column in the data source which should be used for the values.
        node_type: default is chance_node. It can be decision_node and utility_node in influence diagram.
    Outputs:
        model which has chance nodes.
    """
    #states of the node
    states     = data[col].unique() 
    print(f'The states of {col} are:',states)
    len_states = data[col].nunique()
    
    #create an empty label then add labels
    node = gum.LabelizedVariable(col,col,len_states)
    for inx in range(len_states):
        node.changeLabel(inx,states[inx])
        #print('node and its labels',node)
        
    node_types   = ['chance_node','decision_node','utility_node']
    add_node     = False
    if node_type == node_types[0]:
        model.addChanceNode(node)
        add_node = True
    if node_type == node_types[1]:
        model.addDecisionNode(node)
        add_node = True
    if node_type == node_types[2]:
        model.addUtilityNode(node)
        add_node = True
    if add_node:
            print(f'{node_type} {col} has been added in the model!')
    else:
            print(f'The node type should be one from the list {node_types} ')
    #return IDmodel

def add_arc(model,head,tail):
    #TODO:
    # 1. assert that the model has nodes
    # 2. print model nodes name and labels
    # 3. try except when acr already exist.
    """
    Add arcs between two nodes for the model which is created by pyagyrum
    Inputs:
        model: influence diagram or decision network which is created with pyargum.
        head:  head node of the arc.
        tail:  tail node of the arc.
    Outputs:
        
    
    """
    model.addArc(model.idFromName(head), model.idFromName(tail))


def nested_list_to_model(model, nodes_states_dic:dict, nested_list:list, child_node,other_parent_nodes):
    """
    taking the node state dic and its corresponding values from nested list as cpt input 
    for the nodes which have parent nodes in the model which is build with pyagrum.
    the nested list should be split to serval smaller nested list 
    that len(smaller nested list) = len(first_parent_node_states)
    
    Inputs:
          model:            model that is build with pyargume python package.
          nodes_states_dic: it has the child node and its states, first parent node and its state 
                            and other parents nodes and their states combination.
          nested_list:      len(nested_list) is the len(combination of other parent nodes)
                            len(nested_list[0]) is the len(states of first partment node)
                            len(nested_list[0][0]) is the len(states of child node)
          child_node:       P(A|B,C), A is child node
        
          other_parent_nodes: it is the one which is not defined as the input in the model.
                         
    Outputs: Model with updated CPT value
    """
    
    #how many other parent nodes
    len_other_parent_nodes             = len(other_parent_nodes)
    len_other_parent_nodes_state_combi = len(nested_list)
    
    #it is the {'age':'middle_age','BMI':'obesity'} part in 
    #model.cpt(IDmodel.idFromName(child_node))[{'age':'middle_age','BMI':'obesity'}]
    
    
    for inx in range(len_other_parent_nodes):
        #only have the keys
        input_dic = {k:None for k in other_parent_nodes}
        #print(input_dic)
        
    #add values for each key
    all_states_combination = list(nodes_states_dic['other parent nodes'].items())[0][1]

    for inx in range(len_other_parent_nodes_state_combi): 
        states_combi = all_states_combination[inx]

        #update value to the dic keys
        for i in range(len(states_combi)):
            input_dic[other_parent_nodes[i]] = states_combi[i]

        print('input dictionary{ node: node_state } is',input_dic)
        model.cpt(model.idFromName(child_node))[input_dic] = nested_list[inx]
