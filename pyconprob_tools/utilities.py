import numpy as np
import pandas as pd
from functools import reduce

def dataframe_to_dict(data: pd.DataFrame):
    """
    extract states from the crosstabe dataframe
    Inputs:
        data: dataframe. It should be the data = pd.crosstab()
    Outputs:
        dict_states: dictionary, key is the state combination, data is the value of the state combination.
    """
    
    dic                = data.to_dict(orient='split')
    states_combination = dic['index']
    state_data         = dic['data']

    dict_states        =  {k:v for (k,v) in zip(states_combination,state_data)}
        
    return dict_states


def dict_to_nested_list(dic: dict, first_parent_states,other_parents_states_combination):
    #TODO: write the dic keys as return
    """
    extract value from dictory to nested list, group by the key[0] (first element in the key tuple)
    if the state combination exits in the dictionary, exact it in the nested list.
    if the key of the dictionary does not exist in state_combination, set the value of to 0.
    
    Inputs:
        dic:                               dictionary, key should be tuples.
        first_parent_states:               first state (first parent node in model of pyagrum)
        other_parents_states_combination:  all combinations from different states
    Outputs:
        val_nested_list:
            len(val_nested_list[0]) = number of state combination EXCEPT the first state in dict.keys[i]
            len(val_nested_list[1]) = number_of_different_values_in_key[0]
            len(val_nested_list[2]) = len(dic.values)
        dic_keys: 
            nested_list: it should be have the variable name for each value in val_nested_list
            so it can be used for cross-checking when write input for the model.
    """
    
    nested_list = []
    
    len_other_parents_states = len(other_parents_states_combination)
    len_first_parent_states  = len(first_parent_states)
    
    for state_combination in other_parents_states_combination:
        
        first_parent_values = []
        for state in first_parent_states:
            
            dic_key    = tuple([state] + list(state_combination))
            len_data   = len(dic[dic_key])
            value_zero = [0] * len_data
            
            if dic_key in list(dic.keys()):
                first_parent_values.append(dic[dic_key])
                
            else:
                first_parent_values.append(value_zero)
                
        nested_list.append(first_parent_values)
        
    return nested_list

def merge_key(dfs_list):
    """
    Find the key to merge all dataframes, the key should be exist in all dataframe.
    Input: 
        dfs_list
    """
    dfs_cols_list       = [list(df.columns) for df in dfs_list]
    counter             = 0

    #it is enough to check the columns in the first dataframe.
    for col in dfs_cols_list[0]:
        for df_cols in dfs_cols_list:
            if col in df_cols:
                counter += 1
                if counter == len(dfs_cols_list):
                    merge_key = col
                    return merge_key
                    break
            else:
                print('No merge key is found from these dataframes')

def merge_dfs(dfs_list,merge_key):
    """
    merge the dataframes
    Inputs:
        dfs_list: list of all dataframes.
        merge_key: string, the name of the merge key e.g. 'id'

    Output:
        merged_dfs: dataframe
    """
    nan_value   = 0
    merged_dfs  = reduce(lambda left,right: pd.merge(left,right,on=[merge_key],
                                            how='outer'), dfs_list).fillna(nan_value)
    return merged_dfs

