import os
import sys
module_path = os.path.abspath(os.path.join('..'))
#print(module_path)
if module_path not in sys.path:
    sys.path.append(module_path)

import numpy as np
import pandas as pd
import itertools
import functools
from functools import reduce
from collections import Counter

import pyAgrum as gum
import pyAgrum.lib.notebook as gnb

from pyconprob_tools import pyagrum_utilites as agru
from pyconprob_tools import utilities as ut
from pyconprob_tools import node_values as nv

class InfluenceDiagram():
    """
    Build an influence diagram with pyagrum python library. 
    https://pyagrum.readthedocs.io/en/stable/influenceDiagram.html?highlight=influence
    
    Data could be used directly as the input (calucate CPT) for the diagram.

    """

    # class attibute
    

    # instance attibutes
    def __init__(self,dfs,dfs_names,nodes_names):
        
        self.dfs            = dfs
        self.dfs_names      = dfs_names
        self.nodes_names    = nodes_names
        
        # merge key is the key which every dataframe has.
        self.merge_key      = ut.merge_key(self.dfs)

        self.model          = gum.InfluenceDiagram()
        
          
    def add_node(self,df,node,node_type):
        """
        add all chance nodes, decision nodes and utility nodes in the model
        Inputs:
            data       : dataframe or other datasouce
            node       :  string, column name
        Outputs:
            model with new node
        """
        assert node in self.nodes_names, 'Node is not in the attributes.'
        
        agru.add_node(self.model,df,node,node_type = node_type)
    
    def add_arc(self,head,tail):
        """
        add connection (arc/edge) between nodes.
        """
        agru.add_arc(self.model,head,tail)

    
    @property
    def node_name_id(self):
        """
        put the node name and id in dictionary for future query.
        Input:
            self: model.
        Output:
            node_dic: dictionary, value is the name, key is id.
        """
        names     = self.model.names()
        ids       = self.model.nodes()
        node_dict = {ids[i]:names[i] for i in range(len(names))}
        
        return node_dict
    
        
    def add_node_values(self,df,node):
        # TODO:
        # 1. this ONLY for chance node, function for decision_node is needed.
        """
        add values for node.
        Inputs:
            data: dataframe. 
            node:  column of the data
        """
        
        parent_nodes              = self.model.parents(self.model.idFromName(node))
        
        # for the node that does not have any parent node
        if len(parent_nodes)      == 0:
            nv.indep_value(self.model,df,node)
        
        # for the node having parent nodes, first merge dataframes then calcuate the values
        else:
            parent_nodes_names    = [self.node_name_id[key] for key in parent_nodes]
            print(f'The parent nodes of {node} are {parent_nodes_names}')
            
            needed_nodes_names   = parent_nodes_names + [node]
            #remove duplicated dfs
            nodes_df_names = set([self.dfs_names[self.nodes_names.index(node)] for node in needed_nodes_names])
            
            nodes_dfs      = [self.dfs[self.dfs_names.index(df_name)] for df_name in nodes_df_names]
            
            merged_dfs             = ut.merge_dfs(nodes_dfs,self.merge_key)
            
            nodes_states_dic,values_for_cpt = nv.value_from_crosstab(merged_dfs,node,parent_nodes_names)
            print(nodes_states_dic)
            #print(values_for_cpt)
            # parent_nodes_names[1:] does not include the first parent.
            agru.nested_list_to_model(self.model,nodes_states_dic, values_for_cpt,node,parent_nodes_names[1:])