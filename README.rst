=========
pyconprob
=========


.. image:: https://img.shields.io/pypi/v/pyconprob.svg
        :target: https://pypi.python.org/pypi/pyconprob

.. image:: https://img.shields.io/travis/lin3/pyconprob.svg
        :target: https://travis-ci.com/lin3/pyconprob

.. image:: https://readthedocs.org/projects/pyconprob/badge/?version=latest
        :target: https://pyconprob.readthedocs.io/en/latest/?version=latest
        :alt: Documentation Status




Python conditional probability table calculation from pre-processed data.


* Free software: MIT license
* Documentation: https://pyconprob.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
