# The functions in this file are the ones that are needed to calculate the values (frequencies/counts) for 
# conditional propability table

import os
import sys
module_path = os.path.abspath(os.path.join('..'))
#print(module_path)
if module_path not in sys.path:
    sys.path.append(module_path)

import numpy as np
import pandas as pd
import itertools

from pyconprob_tools import utilities as ut

# CPT is only for chance node in influence diagram
def indep_value(model,data,col): 
    #TODO:
    #1, it should be in class parameter so data and col can be re-used here. 
    #2, how to re-write it so that it is not only for one table. if it does not have parent, then use it.
    #3, make sure that the lengh of val (3 unique values match the input)
    #4, would be better use potentials??
    #5. return should be sth else, not model or no return?
    #6. should not the simple sum for total, some pat may have serveal records.
    #7. should round the float.
    
    """
    Calucate the probability distribution for the node (its states' values are not dependent on others):
    1. chance node which does not have any parent node in the model.The node is the data[col].
    2. decision or utility node which value does not depend on other node.

    Input: 
        model: model which is build with pyagrum python package.
        data:  dataframe
        col:   column name. (Assume that the value of the node is from the data[col])
    """
    # state and its counts of the node.
    states_counts    = data[col].value_counts()
    #print('state and its counts:',states_counts)
    # states of the node
    states       = list(states_counts.index)
    len_states   = len(states)
    total        = sum( states_counts)
    
    #labels (states) from the variable(node)
    #label is tuple and it should be same with state
    labels = model.variableFromName(col).labels()
    #print('labels in node:',labels)
    assert set(states) == set(labels),'Number of states from data does not match with the number of labels from node.'
    
    for label in labels:
        # same value has different index in label tuple and state list.
        idx_label = labels.index(label)
        idx_state = states.index(label)
        #print('label:',idx_label,labels[idx_label])
        #print('state:',idx_state,states[idx_state])
        model.cpt(col)[idx_label] = states_counts[idx_state]/total


def value_from_crosstab(data: pd.DataFrame, child_node, parent_nodes):
    """
    calulate the value (counts) for condional probability table (CPT) from pd.crosstab()
    In CPT, P(A|B,C), A is child node, {B,C} are parent nodes.
    
    Inputs:
        data: dataframe, merged dataframe
        child_node: string. 
        parent_nodes: list of parent nodes.
    Outputs:
        keys:        
        nested_list: it should be used in the model which is build with pyargurm directly.
    """
    
    index_list        = [data[inx] for inx in parent_nodes]
    nodes_states_dic  = {}
    
    df                = pd.crosstab(index_list,data[child_node],dropna = False)

    #update the node_states_dic
    child_node_states = data[child_node].unique().tolist()
    nodes_states_dic.update({'child node':{child_node:child_node_states}})
    
    #reorder the columns so that they have some column positions
    df                = df[child_node_states]
    #print('crosstab cols,', df.columns)
    assert list(df.columns) == child_node_states, 'The column names in crosstab are different with the states of child node!'
    
    # defining first node in the parent nodes is important
    # because its len should be same with the input for the model.
    first_parent_node        = parent_nodes[0]
    first_parent_node_states = data[first_parent_node].unique().tolist()
    
    #update the node_states_dic
    nodes_states_dic.update({'first parent node': {first_parent_node:first_parent_node_states}})
    
    other_parent_nodes         = parent_nodes[1:]
    other_parent_nodes_states = []
    
    for node in other_parent_nodes:
        node_states = data[node].unique().tolist()
        other_parent_nodes_states.append(node_states)

    combination_other_parent_nodes_states = list(itertools.product(*other_parent_nodes_states))

    #update the node_states_dic
    nodes_states_dic.update({'other parent nodes': {tuple(other_parent_nodes):combination_other_parent_nodes_states}})
    
    dic         = ut.dataframe_to_dict(df)
    nested_list = ut.dict_to_nested_list(dic, first_parent_node_states,combination_other_parent_nodes_states)
    
    return nodes_states_dic, nested_list