"""Main module."""
# initialize
import os
import sys
module_path = os.path.abspath(os.path.join('..'))
#print(module_path)
if module_path not in sys.path:
    sys.path.append(module_path)
    
import warnings
warnings.filterwarnings('ignore')
    
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pyAgrum as gum
import pyAgrum.lib.notebook as gnb
import seaborn as sns

from IPython.display import display
pd.options.display.max_columns = None
pd.options.display.max_rows = None